// C++ calculator

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int main(void)
{
  // two numbers to hold the numbers entered
  double d_num1, d_num2;
  // string to hold user input
  std::string s_input;
  // vector to hold the calculation string broken up
  std::vector<std::string> s_vec_calc;
  // print prompt to console
  std::cout << "Enter calculation: ";
  // get user input
  getline(std::cin, s_input);
  // create a string stream to break up the calculation
  std::stringstream ss(s_input);
  // variable to hold the spit out word
  std::string s_word;
  // token to split up the words
  char c_space = ' ';

  // inside of the sstream
  // store a word in s_word
  // get the word by reading everything up to a space
  while (getline(ss, s_word, c_space))
  {
    // push the word onto the vector stack
    s_vec_calc.push_back(s_word);
  }

  d_num1 = std::stod(s_vec_calc[0]);
  d_num2 = std::stod(s_vec_calc[2]);

  std::string operation = s_vec_calc[1];
  if (operation == "+")
  {
    std::cout << s_input << " = " << d_num1 + d_num2 << std::endl;
  }
  else if (operation == "-")
  {
    std::cout << s_input << " = " << d_num1 - d_num2 << std::endl;
  }
  else if (operation == "*")
  {
    std::cout << s_input << " = " << d_num1 * d_num2 << std::endl;
  }
  else if (operation == "/")
  {
    std::cout << s_input << " = " << d_num1 / d_num2 << std::endl;
  }
  else
  {
    std::cout << "Please only enter +, -, *, or /" << std::endl;
  }

  // successful execution; exeunt;
  return 0;
}
