// Condiional Operators
// create a program to determine if a birthday is important or not
#include <cstdlib>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

int main(void)
{
  // important birthday if ages: 1-18, 21, 50, 65+

  // prompt the user for an age
  std::cout << "Enter your age: ";
  // create a variable to hold the age entered by the user
  std::string s_age;
  // get an age value from the user
  getline(std::cin, s_age);
  // convert the age to an integer
  int i_age = stoi(s_age);

  // check if the age is important
  if (i_age >= 1 && i_age <= 18)
  {
    std::cout << "Important birthday." << std::endl;
  }
  else if (i_age == 21 || i_age == 50)
  {
    std::cout << "Important birthday." << std::endl;
  }
  else if (i_age >= 65)
  {
    std::cout << "Important birthday." << std::endl;
  }
  else
  {
    std::cout << "Not an important birthday." << std::endl;
  }

  // successful exectution; return;
  return 0;
}
