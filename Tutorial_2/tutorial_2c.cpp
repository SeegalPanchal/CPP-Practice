// Arrays and Vectors

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int main(void)
{
  // arrays
  int i_array[10] = { 1 };
  int i_array2[] = { 1, 2, 3 };
  int i_array3[5] = { 8 , 9 };

  // how to get the size of an array
  // divide the size of the array by the size of each element
  std::cout << "Array size: " << sizeof(i_array3) / sizeof(int) << std::endl;

  // Vectors
  // set 2 spaces aside for the a vector
  std::vector<int> i_vec_rand_num(2);
  i_vec_rand_num[0] = 10;
  i_vec_rand_num[1] = 20;
  // if you want to put an additional item to the vector
  // pushes 30 the end of the vector
  i_vec_rand_num.push_back(30);

  std::cout << "Last element: " << i_vec_rand_num[i_vec_rand_num.size() - 1] << std::endl;

  // Example
  // random sentence
  std::string s_sentence = "This is a sentence.";
  // vector that will hold every word in the sentence
  std::vector<std::string> s_word_vec;
  // split the sentence into each word
  std::stringstream ss(s_sentence);
  // temp variable to hold the word released from the sentence by stringstream
  std::string s_word;
  // token to split the words up
  char c_space = ' ';
  // while the string stream still has words to spit out
  while (getline(ss, s_word, c_space))
  {
    // push it onto the vector stack
    s_word_vec.push_back(s_word);
  }

  for (int i = 0; i < s_word_vec.size(); ++i)
  {
    std::cout << i << ": " << s_word_vec[i] << std::endl;
  }

  // successful execution; return;
  return 0;
}
