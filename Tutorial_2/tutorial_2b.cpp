// Condiional Operators
// create a program to determine if a birthday is important or not
#include <cstdlib>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

int main(void)
{
  // important birthday if ages: 1-18, 21, 50, 65+

  // prompt the user for an age
  std::cout << "Enter your age: ";
  // create a variable to hold the age entered by the user
  std::string s_age;
  // get an age value from the user
  getline(std::cin, s_age);
  // convert the age to an integer
  int i_age = stoi(s_age);
  int i_grade = i_age - 5;

  if (i_grade < 0)
  {
    std::cout << "You are too young for school." << std::endl;
  }
  else if (i_grade == 0)
  {
    std::cout << "You are in Kindergarten." << std::endl;
  }
  else if (i_grade <= 12)
  {
    std::cout << "You are in grade " << i_grade << "." << std::endl;
  }
  else if (i_grade <= 16)
  {
    std::cout << "You are in University." << std::endl;
  }
  else
  {
    std::cout << "You are too old for school." << std::endl;
  }

  // successful exectution; return;
  return 0;
}
