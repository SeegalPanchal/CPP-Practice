// assuming this has stuff to do with mem managements and CMD arguments
#include <cstdlib>
// this is used for standard output
#include <iostream>
// find the minimum and maximum values for data types
#include <limits>
// manipulating string
#include <sstream>
// strings are included in this header file (probably)
#include <string>
// this is for arrays
#include <vector>

int main(void)
{
  // load the prompt into a string
  std::string s_question("Enter the number of miles: ");
  // print out the prompt
  std::cout << s_question;

  // declare a variable to hold the input
  std::string s_input;
  // get user input and load it into the input variable
  getline(std::cin, s_input);

  // create a double variable to hold the input as a double
  double db_miles = stod(s_input);
  // create a double variable to hold the kilometers after calculation
  double db_km = db_miles * 1.60934;

  // The tutorial said to use printf (to format into 4 decimals only)
  // but I like cout, its fun to use
  std::cout << db_miles << " miles is " << db_km << " kilometers." << std::endl;

  // terminate successfully
  return 0;
}
