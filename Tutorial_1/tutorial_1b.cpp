// assuming this has stuff to do with mem managements and CMD arguments
#include <cstdlib>
// this is used for standard output
#include <iostream>

// find the minimum and maximum values for data types
#include <limits>

// manipulating string
#include <sstream>
// strings are included in this header file (probably)
#include <string>
// this is for arrays
#include <vector>

// only need to type cout; similar to using namespace, but only for cout
using std::cout;
// endl means pass a new line !!
using std::endl;

// prototyping required in C++
void print_variable_size();

// using command line arguments
int main(void)
{
  // examples of primitive data types
  bool b_married = false;
  char ch_my_grade = 'A';
  unsigned short int usi_Age = 18;
  short int si_weight = 140;
  unsigned int ui_days = 365;
  int i_week = 7;
  long l_big_number = 1234567890;
  float f_pi = 3.14159;
  double db_big_float = 1.1234567890123456890;
  long double ld_pi = 3.1415926535;
  auto what_is_this = true;

  // function that prints the min/max values
  // that can be held by the primitve data types
  // print_variable_size();

  // defining a string using std
  std::string s_question("Enter Number 1: ");
  // the above and below line are similar (below one is easier to read)
  std::string s_num1, s_num2;
  // print the question
  std::cout << s_question;
  // get line gets user input
  getline(std::cin, s_num1);
  std::cout << "Enter Number 2: ";
  getline(std::cin, s_num2);

  // stoi is basically atoi
  // stands for "string to integer"
  int i_num1 = std::stoi(s_num1);
  int i_num2 = std::stoi(s_num2);

  std::cout << i_num1 << " + " << i_num2 << " = " << i_num1 + i_num2 << endl;

  // return 0 as "successful execution"
  return 0;
}

void print_variable_size() {
  // to find the size of a boolean
  std::cout << "Min bool: " << std::numeric_limits<bool>::min() << endl;
  std::cout << "Max bool: " << std::numeric_limits<bool>::max() << endl;
  std::cout << endl;

  // to find the size of a char - had to cast for char only?
  std::cout << "Min char: " << (int) std::numeric_limits<char>::min() << endl;
  std::cout << "Max char: " << (int) std::numeric_limits<char>::max() << endl;
  std::cout << endl;

  // to find the size of a unsigned short in
  std::cout << "Min unsigned short int: " << std::numeric_limits<unsigned short int>::min() << endl;
  std::cout << "Max unsigned short int: " << std::numeric_limits<unsigned short int>::max() << endl;
  std::cout << endl;

  // to find the size of a short int
  std::cout << "Min short int: " << std::numeric_limits<short int>::min() << endl;
  std::cout << "Max short int: " << std::numeric_limits<short int>::max() << endl;
  std::cout << endl;

  // to find the size of a unsigned int
  std::cout << "Min unsigned int: " << std::numeric_limits<unsigned int>::min() << endl;
  std::cout << "Max unsigned int: " << std::numeric_limits<unsigned int>::max() << endl;
  std::cout << endl;

  // to find the size of a int
  std::cout << "Min int: " << std::numeric_limits<int>::min() << endl;
  std::cout << "Max int: " << std::numeric_limits<int>::max() << endl;
  std::cout << endl;

  // to find the size of a long
  std::cout << "Min long: " << std::numeric_limits<long>::min() << endl;
  std::cout << "Max long: " << std::numeric_limits<long>::max() << endl;
  std::cout << endl;

  // to find the size of a float
  std::cout << "Min float: " << std::numeric_limits<float>::min() << endl;
  std::cout << "Max float: " << std::numeric_limits<float>::max() << endl;
  std::cout << endl;

  // to find the size of a double
  std::cout << "Min double: " << std::numeric_limits<double>::min() << endl;
  std::cout << "Max double: " << std::numeric_limits<double>::max() << endl;
  std::cout << endl;

  // to find the size of a long double
  std::cout << "Min long double: " << std::numeric_limits<long double>::min() << endl;
  std::cout << "Max long double: " << std::numeric_limits<long double>::max() << endl;
}
