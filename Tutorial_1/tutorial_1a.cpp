// assuming this has stuff to do with mem managements and CMD arguments
#include <cstdlib>
// this is used for standard output
#include <iostream>
// manipulating string
#include <sstream>
// strings are included in this header file (probably)
#include <string>
// this is for arrays
#include <vector>

// not using namespace because function conflicts with functions in the std namespace
// using namespace std;

// only need to type cout; similar to using namespace, but only for cout
using std::cout;
// endl means pass a new line !!
using std::endl;

// using command line arguments
int main(int argc, char **argv)
{
  // cout represents the console / screen
  // << is a "stream insertion operator", sends stuff to the write to the standard output stream
  std::cout << "hello, world" << std::endl;

  // if the user entered more than just the command to run the file
  if (argc != 1)
  {
    std::cout << "You entered " << argc << " arguments." << std::endl;
  }

  // loop through the arguments passed into the program during execution
  for (int i = 0; i < argc; ++i)
  {
    // print out the arguments that were passed in
    std::cout << argv[i] << endl;
  }

  // return 0 as "successful execution"
  return 0;
}
