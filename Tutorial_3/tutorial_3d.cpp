#include <iostream>
#include <numeric>
#include <vector>

// assignment using pointers
void Assign_Age(int *age);
void DoubleArray(int *arr, int size);

int main(void)
{
  int age = 43;
  Assign_Age(&age);
  std::cout << "Age: " << age << std::endl;
  // successful execution; exeunt;

  //double the array
  int arr[] = { 1, 2, 3 };
  DoubleArray(arr, 3);
  for (int i = 0; i < 3; ++i)
  {
    std::cout << arr[i] << std::endl;
  }

  return 0;
}

void Assign_Age(int *age)
{
  *age = 24;
}

void DoubleArray(int *arr, int size)
{
  for (int i = 0; i < size; ++i)
  {
    arr[i] *= 2;
  }
}
