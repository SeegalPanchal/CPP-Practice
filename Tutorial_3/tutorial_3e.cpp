#include <iostream>
#include <numeric>
#include <vector>

std::vector<int> Range(int start, int end, int increment);

int main(void)
{
  // calculate compounding interest on investment
  double d_investment;
  double d_rate;

  std::cout << "How much do you want to invest? ";
  std::cin >> d_investment;
  std::cout << "What is the interest rate? ";
  std::cin >> d_rate;
  // the rate is the 1 + (the rate in decimal)
  // e.g. 25% == 0.25, and the multiplier is 1.25, or 1 + 0.25
  d_rate = 1 + (d_rate/100);

  for (int i: Range(1, 10, 1))
  {
    d_investment *= d_rate;
  }
  std::cout << "Your investment value after 10 years is $" << d_investment << "." << std::endl;

  // successful execution; exeunt;
  return 0;
}

std::vector<int> Range(int start, int end, int increment)
{
  std::vector<int> new_vector;
  while (start <= end)
  {
    new_vector.push_back(start);
    start += increment;
  }
  return new_vector;
}
