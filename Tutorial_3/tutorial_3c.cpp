#include <iostream>
#include <numeric>
#include <vector>

double Add_Num(double num1, double num2);

int main(void)
{
  double num1, num2;
  // print out a prompt
  std::cout << "Enter number 1: ";
  // push user entry to the variable number 1 (hence >> arrows)
  std::cin >> num1;
  std::cout << "Enter number 2: ";
  std::cin >> num2;

  // add the two numbers:
  std::cout << num1 << " + " << num2 << " = " << Add_Num(num1, num2) << std::endl;

  // successful execution; exeunt;
  return 0;
}

// adds two double values together
double Add_Num(double num1, double num2)
{
  return (num1 + num2);
}
