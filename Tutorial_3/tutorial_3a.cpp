#include <iostream>
#include <numeric>
#include <vector>

int main(void)
{
  std::vector<int> i_vec(10);
  // fill the range (first --> last, start)
  // with the last value being where you started from
  // with increasing integers (++Z)
  std::iota(std::begin(i_vec), std::end(i_vec), 0);
  for (int i = 0; i < i_vec.size(); ++i)
  {
    std::cout << i_vec[i] << std::endl;
  }
  // abbreviated for loop
  for (int y: i_vec)
  {
    std::cout << i_vec[y] << std::endl;
  }

  int i_val = 8;
  if (i_val % 2 == 0)
  {
    std::cout << i_val << " is even." << std::endl;
  }
  else
  {
    std::cout << i_val << " is odd." << std::endl;
  }
  // successful execution; exeunt;
  return 0;
}
