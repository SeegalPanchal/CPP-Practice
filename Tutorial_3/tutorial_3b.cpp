#include <iostream>
#include <numeric>
#include <vector>

int main(void)
{
  std::vector<int> i_vec(10);
  // fill the range (first --> last, start)
  // with the last value being where you started from
  // with increasing integers (++Z)
  std::iota(std::begin(i_vec), std::end(i_vec), 1);
  // cycle through the vector
  for (int y: i_vec)
  {
    if (i_vec[y] % 2 == 0)
    {
      std::cout << i_vec[y] << " is even." << std::endl;
    }
  }
  // successful execution; exeunt;
  return 0;
}
