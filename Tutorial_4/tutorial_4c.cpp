#include <ctime>
#include <iostream>
#include <vector>

std::vector<int> Range(int start, int end, int increment);

int main(void)
{

  // successful execution; exeunt;
  return 0;
}

// Python like range function
std::vector<int> Range(int start, int end, int increment)
{
  std::vector<int> new_vector;
  while (start <= end)
  {
    new_vector.push_back(start);
    start += increment;
  }
  return new_vector;
}
