#include <iostream>
#include <vector>

std::vector<int> Range(int start, int end, int increment);

int main(void)
{
  double d_num1, d_num2;
  std::cout << "Enter number 1: ";
  std::cin >> d_num1;
  std::cout << "Enter number 2: ";
  std::cin >> d_num2;

  try
  {
    if (d_num2 == 0)
    {
      throw std::runtime_error("Error Occurred");
    }
    std::cout << d_num1 << " / " << d_num2 << " = " << d_num1 / d_num2 << std::endl;
  }
  catch (std::exception &exc)
  {
    std::cout << "Handled Exception: " << exc.what() << std::endl;
  }
  // catch ALL (handle all other exception)
  catch (...)
  {
    std::cout << "Default Exception." << std::endl;
  }

  // successful execution; exeunt;
  return 0;
}

// Python like range function
std::vector<int> Range(int start, int end, int increment)
{
  std::vector<int> new_vector;
  while (start <= end)
  {
    new_vector.push_back(start);
    start += increment;
  }
  return new_vector;
}
