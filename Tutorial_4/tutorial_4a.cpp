#include <iostream>
#include <vector>

std::vector<int> Range(int start, int end, int increment);
void Print_Row(int i_spaces, int i_hashes);

int main(void)
{
  int i_rows;
  int i_spaces;
  std::cout << "Enter the number of rows: ";
  std::cin >> i_rows;

  // the first row will have 4 spaces (if 5 is inputted)
  i_spaces = i_rows-1;

/*
  1. Decrement spaces by one each time through loop
  2. Increment the hashes by 2 each time through loop
  3. Save spaces to the stump by calculating tree height -1
  4. Decrement from tree height until equals zero
  5. Print spaces and then hashes for each rows
  6. Print stump spaces and then 1 hash
*/
  int i_hashes = 1;
  while (i_spaces >= 0)
  {
    // print 2 more hashes every loop
    Print_Row(i_spaces, i_hashes);
    // decrement the number of spaces
    --i_spaces;
    i_hashes += 2;
  }
  Print_Row((i_rows-1), 1);

  // successful execution; exeunt;
  return 0;
}

void Print_Row(int i_spaces, int i_hashes)
{
  // print out "i_spaces" number of spaces
  for (int j = 0; j < i_spaces; ++j)
  {
    std::cout << " ";
  }
  // print the number of hashes after printing spaces
  for (int k = i_hashes; k > 0; --k)
  {
    std::cout << "#";
  }
  //print a new line at the end
  std::cout << std::endl;
}

// Python like range function
std::vector<int> Range(int start, int end, int increment)
{
  std::vector<int> new_vector;
  while (start <= end)
  {
    new_vector.push_back(start);
    start += increment;
  }
  return new_vector;
}
